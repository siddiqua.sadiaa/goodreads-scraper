import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
from time import sleep
import re
import csv

interest = [
    'Biography', 'Business', 'Classics', 'Fantasy',
    'Fiction', 'History', 'Memoir', 'Mystery',
    'Nonfiction', 'Science Fiction', 'Self Help'
]
categories_and_links = []


# Function to generate full URL
def create_full_url(url='https://www.goodreads.com/'):
    # fake-useragent helps to avoid being detected as bot by adding header data (browser)


    ua = UserAgent(use_cache_server=False, cache=False, verify_ssl=False)
    hdr = {'User-Agent': ua.random,
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'
           }
    url_base = 'https://www.goodreads.com/'
    request_url = url_base + url
    if url == url_base:
        res = requests.get(url, headers=hdr)
    else:
        res = requests.get(request_url, headers=hdr)
    return res


# create soup object
def create_soup_object(res):
    soup = BeautifulSoup(res.text, 'html.parser')
    return soup


# get link for each element of interest and then append name & link in new list categories and links
def get_links_for_categories_of_interest():
    res = create_full_url()
    soup = create_soup_object(res)
    soup_links = soup.find(id='browseBox').find_all('a', class_="gr-hyperlink")

    for tag in soup_links:
        for item in interest:
            if item == tag.get_text():
                global categories_and_links
                categories_and_links.append({'name': item, 'link': tag['href']})
    return categories_and_links


get_links_for_categories_of_interest()


# print(categories_and_links)
'''
def main_page_soup():
    all_books = []
    for index, item in enumerate(categories_and_links):
        url = item['link']
        name = item['name']
        sleep(2)

        # request to the landing page
        res_category = create_full_url(url)
        soup = create_soup_object(res_category)
        main_page_url = soup.find('a', text=name + ' ' + 'Books')['href']

        # request to the main page
        sleep(2)
        res_main_page = create_full_url(main_page_url)
        soup_main_page = create_soup_object(res_main_page)

        # scraping data

        books = soup_main_page.find_all('div', class_='left')
        all_books.append(books)
    print(len(all_books))
    print(len(all_books[0]))

    return all_books
'''
def get_book_list():
    all_books = []
    for index, item in enumerate(categories_and_links):
        url = item['link']
        name = item['name']
        sleep(2)

        # request to the landing page
        res_category = create_full_url(url)
        soup = create_soup_object(res_category)
        main_page_url = soup.find('a', text=name + ' ' + 'Books')['href']

        # request to the main page
        sleep(2)
        res_main_page = create_full_url(main_page_url)
        soup_main_page = create_soup_object(res_main_page)

        # scraping data

        soup_books = soup_main_page.find_all('div', class_='left')
        all_books.append({'category':name,'soup_books':soup_books})
    print(len(all_books))
    print(len(all_books[0]))

    return all_books


#main_page_soup()


def scraping_data_and_writing():

    # prepare txt doc to write data
    with open('goodreads.txt', 'w') as file:
        txt_writer = csv.writer(file)
        txt_writer.writerow(['title', 'category', 'writer','shelved_num','avg_rating','total_ratings','published_year'])

        # get soup object of main page
        book_list = get_book_list()
        #type(book_list)
        #type(book_list['cat_books'])
        print(len(book_list))

        # loop through books of main page to get data on each category
        for each_dict in book_list:

            # get category
            category = each_dict['category']

            for soup_book in each_dict['soup_books']:
                # get title
                my_title = soup_book.find(class_="bookTitle").get_text()
                title = re.sub(r' \(.+\)', r'', my_title)

                # get writer
                writer = soup_book.find(class_="authorName").get_text()

                # get shelved number
                shelved_text = soup_book.find('a',class_="smallText").get_text()
                shelved_no_parenthesis = shelved_text[1:-1]
                shelved_num = shelved_no_parenthesis.split(' ')[1]

                # get avg_rating
                last_span = str(soup_book.contents[-2].contents[0])
                avg_rating_part = last_span.split('\n')[1]
                avg_rating_part_without_space = avg_rating_part.strip()
                avg_rating = avg_rating_part_without_space.split(' ')[2]

                # get total_ratings
                total_ratings_part = last_span.split('\n')[2]
                total_ratings_part_without_space = total_ratings_part.strip()
                total_ratings_string = total_ratings_part_without_space.split(' ')[0]
                total_ratings = total_ratings_string.replace(',','')


                # get published year
                published_year_part = last_span.split('\n')[3]
                published_year_part_without_space = published_year_part.strip()
                published_year = published_year_part_without_space.split(' ')[1]
                print(title, category, writer, shelved_num, avg_rating, total_ratings, published_year)

                # write data
                txt_writer.writerow([title, category, writer, shelved_num, avg_rating, total_ratings, published_year])


    #return title, writer, shelved_num, avg_rating, total_ratings, published_year


scraping_data_and_writing()
print('Job Done')



